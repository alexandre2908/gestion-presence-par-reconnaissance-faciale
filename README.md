# Attendance System Using Facial Recognition

Some companies ask their employees to point out their presence when they arrive and this can be annoying in one way or another because it is done manually 

this project creates a attendance system based on facial recognition so there is no need to do anything just to present yourself in front of the camera and your attendance will be taken and the time you arrive will be taken.
here is some features:
    
  - Pointing presence on arrive and before leaving
  - Account management
  - Seeing number of hours in a month
  - etc
  

# Future Features!

  - Adding more control for Account
  - Calculate hourly base salary
  - Improve UI and UX

# Getting Started

Just clone the project using
```sh
https://gitlab.com/alexandre2908/gestion-presence-par-reconnaissance-faciale.git
```
and then open it in Android studio and then customize it as you want


Here is the client app : 

```sh 
git clone https://gitlab.com/alexandre2908/TaxiAsk_Client.gi
```


# Prerequisite
- First install python 2.7
- and then opencv to python
- install flask
- Have a good web came for accuracy

### Author


* [Axel Mwenze](https:axelmwenze.com) - Software Engineer and Fullstack Web Developper


License
----

MIT


**Free Software, Hell Yeah!**
