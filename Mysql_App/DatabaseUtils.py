from Database_Connect import Database
import MySQLdb
import time


class DatabaseUtils:
    database_connect = Database().get_connection(db="sncc_db")

    def find_all(self, table_name):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM {0}".format(table_name))
        data = cursor.fetchall()
        return_data = []
        for rows in data:
            return_data.append(rows)
        cursor.close()
        return return_data

    def login_verify(self, username, password):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM agent WHERE nom ='{0}' AND password_agent='{1}'".format(username, password))
        user = cursor.fetchone()
        return user

    def login_verify_Admin(self, username, password):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM administrateur WHERE nomutilisateur ='{0}' AND motdepasse='{1}'".format(username, password))
        user = cursor.fetchone()
        return user

    def get_one_agent(self, userid):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM agent WHERE idagent='{0}'".format(userid))
        user = cursor.fetchone()
        cursor.close()
        return user

    def get_one_admin(self, userid):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM administrateur WHERE idadministrateur='{0}'".format(userid))
        user = cursor.fetchone()
        cursor.close()
        return user

    def get_last_user(self):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM agent WHERE idagent=(SELECT max(idagent) FROM agent)")
        user = cursor.fetchone()
        cursor.close()
        return user

    def supprimer_agent(self, id_agent):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("DELETE FROM agent WHERE idagent={0}".format(id_agent))
        self.database_connect.commit()
        cursor.close()

    def ajouter_agent(self, agent):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        cursor.execute("INSERT INTO agent(nom, postnom, prenom, datenaissance, etatcivil, dateengagement, password_agent, adresse_agent, sexe_agent) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')".format(agent.nom, agent.postnom, agent.prenom, agent.datenaissance, agent.etatcivil, agent.dateengagement, agent.password_agent, agent.adresse_agent, agent.sexe_agent))
        self.database_connect.commit()
        cursor.close()

    def pointer_presence(self, agent):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        date_today = time.strftime('%d/%m/%Y')
        heure_now = time.strftime('%H:%M')
        idagent = int(agent["idagent"])
        cursor.execute("INSERT INTO pointage(heurearrive, date_aujourdhui, idagent) VALUES ('{0}','{1}',{2})".format(heure_now, date_today, idagent))
        self.database_connect.commit()
        cursor.close()

    def verifier_marquage(self, agent):
        cursor = self.database_connect.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        date_today = time.strftime('%d/%m/%Y')
        agent_id = int(agent['idagent'])
        cursor.execute("select * from pointage WHERE date_aujourdhui='{0}' AND idagent={1}".format(date_today, agent_id))
        pointage = cursor.fetchone()
        cursor.close()
        if pointage is None:
            pointage = False
        else:
            pointage = True
        return pointage
