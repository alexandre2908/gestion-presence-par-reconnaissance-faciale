import MySQLdb

from Mysql_App.Singleton import Singleton


@Singleton
class Database:
    connection = None

    def get_connection(self, host="localhost", username="root", password="", db="jeux_test"):
        try:
            if self.connection is None:
                self.connection = MySQLdb.connect(host=host, user=username, passwd=password, db=db)
            return self.connection
        except MySQLdb.OperationalError as op:
            print("Veuillez verifier si voytre serveur est lance")
            exit(0)