
$(document).ready(function () {
    $("#form_ctrl").submit(function () {
        $('#field').hide();
        var name = $('#name_ct').val();
        var mail = $('#mail_ct').val();
        var subject = $('#subject_ct').val();
        var message = $('#msg_ct').val();
        $.post('../badzonestudios/controllers/contact_ctrl.php', {name_ct: name, mail_ct: mail, subject_ct: subject , msg_ct: message}, function (recup){
            $("#field").html(recup).fadeIn();
            $('#name_ct').val('');
            $('#mail_ct').val('');
            $('#subject_ct').val('');
            $('#msg_ct').val('');
            $('#field').delay(3000).fadeOut();

        });
        return false;
    });

    $("#Newsletter_ctrl").submit(function () {
        $('#field_Nltr').hide();
        var mail = $('#mail_lt').val();
        $.post('../badzonestudios/controllers/subcriber_ctrl.php', { mail_lt: mail}, function (recup){
            $("#field_Nltr").html(recup).fadeIn();
            $('#mail_lt').val('');
            $("#field_Nltr").delay(3000).fadeOut();

        });
        return false;
    });

});

