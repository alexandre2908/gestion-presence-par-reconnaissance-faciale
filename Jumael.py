import locale
import time

from flask import (
    flash, g, redirect, render_template, request, session, url_for, Flask
)

from Mysql_App.DatabaseUtils import DatabaseUtils
from Reconaissance_Facial.Classes.Agents import Agents
from Reconaissance_Facial.Face_Capture_With_Rotate import Detection_Write
from Reconaissance_Facial.Recogniser_Video_EigonFace import EigenFaceRecognizer
from Reconaissance_Facial.Trainer_All import trainer_all
import Reconaissance_Facial.Haar_cascade_call as callabables

app = Flask(__name__)
utils = DatabaseUtils()
app.secret_key = "jumael_application"
locale.setlocale(locale.LC_TIME, '')
writing = Detection_Write()
eigenface = EigenFaceRecognizer()
trainer = trainer_all()


@app.route('/')
def index_page():
    return render_template("index.html")


@app.route('/admin_login')
def admin_log():
    return render_template("auth/admin_log.html")


@app.route("/agents_login")
def agents_log():
    return render_template("auth/agents_log.html")


@app.route("/admin_main")
def admin_main():
    return render_template('Admin_Pages/main_admin.html')


@app.route("/modifier_agent")
def modifier_agent():
    return render_template('Admin_Pages/modifier_agent.html')


@app.route("/donnees_bio")
def donnees_bio():
    return render_template('Admin_Pages/Donnees_Bio.html')


@app.route("/agents_main")
def agents_main():
    return render_template('Agents_Pages/main_agents.html')


@app.route("/pointer_agent")
def pointer_presence():
    date_today = time.strftime('%A %d/%m/%Y')
    heure_now = time.strftime('%H:%M')
    is_marked = utils.verifier_marquage(g.agent)

    return render_template('Agents_Pages/pointer_presence.html',
                           today_date=date_today, hour_now=heure_now, is_mark=is_marked
                           )


@app.route('/login_agent_process', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = utils.login_verify(username, password)
        error = None

        if user is None:
            error = "Nom d'utilisateur ou mots de passe incorrect"

        if error is None:
            session.clear()
            session['agent_id'] = user['idagent']
            return redirect(url_for('pointer_presence'))

        flash(error)

    return render_template('auth/agents_log.html')


@app.route('/ajouter_agent', methods=('GET', 'POST'))
def ajouter_agent():
    return render_template('Admin_Pages/ajouter_agents.html')


@app.route('/login_admin_process', methods=('GET', 'POST'))
def login_admin():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = utils.login_verify_Admin(username, password)
        print(user)
        error = None

        if user is None:
            error = "Nom d'utilisateur ou mots de passe incorrect"

        if error is None:
            session.clear()
            session['admin_id'] = user['idadministrateur']
            return redirect(url_for('gestion_agent'))

        flash(error)

    return render_template('auth/admin_log.html')


@app.route('/ajout_action', methods=['POST'])
def ajout_action():
    if request.method == 'POST':
        nom = request.form['nom']
        postnom = request.form['postnom']
        prenom = request.form['prenom']
        adresse = request.form['adresse']
        date_naiss = request.form['date_naiss']
        date_eng = request.form['date_eng']
        etat_civile = request.form['etat_civile']
        password = request.form['password']
        sexe_agent = request.form['sexe_agent']

        agent = Agents(nom, postnom, prenom, date_naiss, etat_civile, date_eng, password, adresse, sexe_agent)
        utils.ajouter_agent(agent)
        agent = utils.get_last_user()
        writing.detectAndWrite(agent)
        trainer.getImageWithID(callabables.getdataset_path())
    return render_template('Admin_Pages/gestion_agent.html')


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index_page'))


@app.before_request
def load_logged_in_user():
    admin_id = session.get('admin_id')
    if admin_id is None:
        g.admin = None
    else:
        g.admin = utils.get_one_admin(admin_id)


@app.before_request
def load_user():
    user_id = session.get('agent_id')
    if user_id is None:
        g.agent = None
    else:
        g.agent = utils.get_one_agent(user_id)


@app.route("/marquer_presence")
def marquer_presence():
    reponse = ""
    agent = eigenface.reconnise_face()
    if agent is None:
        reponse = "visage non trouvee"
    else:
        utils.pointer_presence(agent)
        reponse = "Votre presence a bien etait prise"
    flash(reponse)
    return redirect(url_for('pointer_presence'))


@app.route("/gestion_agent")
def gestion_agent():
    return render_template("Admin_Pages/gestion_agent.html", list_agent=utils.find_all("agent"))


@app.route("/<int:id>/supprimer_agent")
def supprimer_agent(id):
    utils.supprimer_agent(id)
    return render_template("Admin_Pages/gestion_agent.html", list_agent=utils.find_all("agent"))


if __name__ == '__main__':
    app.run()
