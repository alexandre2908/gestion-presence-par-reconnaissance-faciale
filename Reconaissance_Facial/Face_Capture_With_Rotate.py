import os
import cv2  # Importing the opencv
import numpy as np  # Import Numarical Python
import NameFind
import Haar_cascade_call


class Detection_Write:
    WHITE = [255, 255, 255]
    direname = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(direname, 'dataSet')
    #   import the Haar cascades for face ditection
    face_cascade = cv2.CascadeClassifier(Haar_cascade_call.getting_fontalface())
    eye_cascade = cv2.CascadeClassifier(Haar_cascade_call.getting_eyes())

    def __init__(self):
        pass

    def detectAndWrite(self, agent):
        ID = int(agent['idagent'])
        Count = 0
        cap = cv2.VideoCapture(0)  # Camera object

        while Count < 25:
            ret, img = cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert the Camera to graySe
            if np.average(gray) > 110:  # Testing the brightness of the image
                faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)  # Detect the faces and store the positions
                for (x, y, w, h) in faces:  # Frames  LOCATION X, Y  WIDTH, HEIGHT
                    FaceImage = gray[y - int(h / 2): y + int(h * 1.5),
                                x - int(x / 2): x + int(w * 1.5)]  # The Face is isolated and cropped
                    Img = (NameFind.DetectEyes(FaceImage))
                    cv2.putText(gray, "visage detecte", (x + (w / 2), y - 5), cv2.FONT_HERSHEY_DUPLEX, .4, self.WHITE)
                    if Img is not None:
                        frame = Img  # Show the detected faces
                    else:
                        frame = gray[y: y + h, x: x + w]
                    cv2.imwrite(self.filename + "/agent." + str(ID) + "." + str(Count) + ".jpg", frame)
                    cv2.waitKey(300)
                    cv2.imshow("photo capturee", frame)  # show the captured image
                    Count = Count + 1
            cv2.imshow('Fenetre de capture de visage', gray)  # Show the video
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        print 'la capture a prie fin'
        cap.release()
        cv2.destroyAllWindows()
        return 1
