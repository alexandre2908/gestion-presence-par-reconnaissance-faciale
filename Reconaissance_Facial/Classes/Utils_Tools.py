import os
import sys


class Utils_Tools:
    def __init__(self):
        pass

    @staticmethod
    def Get_Haar_face():
        return "Haar/haarcascade_frontalface_default.xml"

    @staticmethod
    def Get_Haar_eye():
        return  "Haar/haarcascade_eye.xml"