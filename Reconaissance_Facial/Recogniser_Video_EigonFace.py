import cv2  # Importing the opencv
import NameFind
import Haar_cascade_call as cascade
from Mysql_App.DatabaseUtils import DatabaseUtils


class EigenFaceRecognizer:
    #   import the Haar cascades for face and eye ditection
    utils = DatabaseUtils()

    face_cascade = cv2.CascadeClassifier(cascade.getting_fontalface())
    eye_cascade = cv2.CascadeClassifier(cascade.getting_eyes())

    recognise = cv2.face.EigenFaceRecognizer_create(15, 4000)  # creating EIGEN FACE RECOGNISER
    recognise.read(cascade.getting_eigenface_Recognizer())  # Load the training data

    def __init__(self):
        pass

    def reconnise_face(self):
        # -------------------------     START THE VIDEO FEED ------------------------------------------
        identity = None
        cap = cv2.VideoCapture(0)  # Camera object
        # cap = cv2.VideoCapture('TestVid.wmv')   # Video object
        ID = 0
        count = 0
        while count < 20:
            ret, img = cap.read()  # Read the camera object
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert the Camera to gray
            faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)  # Detect the faces and store the positions
            for (x, y, w, h) in faces:  # Frames  LOCATION X, Y  WIDTH, HEIGHT
                # ------------ BY CONFIRMING THE EYES ARE INSIDE THE FACE BETTER FACE RECOGNITION IS GAINED ------------------
                gray_face = cv2.resize((gray[y: y + h, x: x + w]), (110, 110))  # The Face is isolated and cropped
                eyes = self.eye_cascade.detectMultiScale(gray_face)
                for (ex, ey, ew, eh) in eyes:
                    ID, conf = self.recognise.predict(gray_face)  # Determine the ID of the photo
                    if ID > 0:
                        identity = self.utils.get_one_agent(ID)
                    else:
                        identity = None
                    # NAME = NameFind.ID2Name(ID, conf)
                    # NameFind.DispID(x, y, w, h, NAME, gray)
            cv2.imshow('EigenFace Face Recognition System', gray)  # Show the video
            count += 1
        cap.release()
        cv2.destroyAllWindows()
        return identity