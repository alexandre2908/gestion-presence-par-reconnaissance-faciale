import os  # importing the OS for path
import cv2  # importing the OpenCV library
import numpy as np  # importing Numpy library
from PIL import Image  # importing Image library
import Haar_cascade_call


class trainer_all:
    EigenFace = cv2.face.EigenFaceRecognizer_create(15)  # creating EIGEN FACE RECOGNISER
    # FisherFace = cv2.face.FisherFaceRecognizer_create(12)  # Create FISHER FACE RECOGNISER
    LBPHFace = cv2.face.LBPHFaceRecognizer_create(1, 1, 7, 7)  # Create LBPH FACE RECOGNISER

    path = 'dataSet'  # path to the photos

    def getImageWithID(self, path):
        imagePaths = [os.path.join(path, f) for f in os.listdir(path)]
        FaceList = []
        IDs = []
        for imagePath in imagePaths:
            faceImage = Image.open(imagePath).convert('L')  # Open image and convert to gray
            faceImage = faceImage.resize((110, 110))  # resize the image so the EIGEN recogniser can be trained
            faceNP = np.array(faceImage, 'uint8')  # convert the image to Numpy array
            ID = int(os.path.split(imagePath)[-1].split('.')[1])  # Retreave the ID of the array
            FaceList.append(faceNP)  # Append the Numpy Array to the list
            IDs.append(ID)  # Append the ID to the IDs list
            cv2.imshow('Training Set', faceNP)  # Show the images in the list
            cv2.waitKey(1)
            ids_lists = np.array(IDs)
            FaceList_all = FaceList

            # ------------------------------------ TRAING THE RECOGNISER ----------------------------------------
        print('TRAINING......')
        self.EigenFace.train(FaceList_all, ids_lists)  # The recongniser is trained using the images
        print('EIGEN FACE RECOGNISER COMPLETE...')
        self.EigenFace.save(Haar_cascade_call.getting_eigenface_Recognizer())
        print('FILE SAVED..')
        cv2.destroyAllWindows()
