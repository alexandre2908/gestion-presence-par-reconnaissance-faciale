import os

direname = os.path.dirname(os.path.abspath(__file__))


def getting_fontalface():
    filename = os.path.join(direname, 'Haar/haarcascade_frontalface_alt2.xml')
    return filename


def getdataset_path():
    filename = os.path.join(direname, 'dataSet')
    return filename


def getting_eyes():
    filename = os.path.join(direname, 'Haar/haarcascade_eye.xml')
    return filename


def getting_eyeglace():
    filename = os.path.join(direname, 'Haar/haarcascade_eye_tree_eyeglasses.xml')
    return filename


def getting_eigenface_Recognizer():
    filename = os.path.join(direname, 'Recogniser/trainingDataEigan.xml')
    return filename
