import cv2  # Importing the opencv
import NameFind
import Haar_cascade_call as haar


# import the Haar cascades for face and eye ditection

class Detector_Video:
    def __init__(self):
        pass

    @staticmethod
    def launch_video_capture():

        face_cascade = cv2.CascadeClassifier(haar.getting_fontalface())
        eye_cascade = cv2.CascadeClassifier(haar.getting_eyes())

        camera = cv2.VideoCapture(0)

        while True:
            ret, img = camera.read()
            noir_blanc = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert the Camera to gray

            # ---------------------------------- FACE DETECTION ------------------------------------

            faces = face_cascade.detectMultiScale(noir_blanc, 1.3, 5)  # Detect the faces and store the positions
            for (x, y, w, h) in faces:  # Frames  LOCATION X, Y  WIDTH, HEIGHT
                gray_face = cv2.resize((noir_blanc[y: y + h, x: x + w]), (110, 110))  # The Face is isolated and cropped
                eyes = eye_cascade.detectMultiScale(gray_face)
                for (ex, ey, ew, eh) in eyes:
                    NameFind.draw_box(noir_blanc, x, y, w, h)

            cv2.imshow('Detection facial', noir_blanc)  # Show the video
            if cv2.waitKey(1) & 0xFF == ord('q'):  # Quit if the key is Q
                break

        cv2.destroyAllWindows()
